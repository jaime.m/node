
const express = require('express')
const router = express.Router()
const Joi = require('joi')
const mongoose = require('mongoose')
const {Schema} = mongoose

const Genres =mongoose.model('genre',new Schema({
    name:{type:String,required:true,minlength:5,maxlength:50}
}))

 router.get('/',async (req,res)=>{
    return await  res.send(Genres)
 })
 
 router.get('/:id',async (req,res)=>{
 let genre = await Genres.findById(req.params.id)
 if (genre)
     res.send(genre)
     else
     return res.status(404).send({
         status:404,
         message:"the genre cannot be found"
     })
 })
 
 router.post('/',async (req,res)=>{
 
     const schema ={
         name:Joi.string().min(3).required()
     }
     const result= Joi.validate(req.body, schema)
     if(result.error){
         res.status(400).send(result.error.details[0].message)
         return
     }
     const genre = new Genres({
         name:req.body.name
     })

     genre = await  genre.save()
    res.send(genre)
 })
 router.put('/:id', async (req,res)=>{
 
 const {error} = validategenre(req.body)
     if(error){
         res.status(400).send(error.details[0].message)
         return
     }
 
     let genre =Genres.findByIdAndUpdate(req.params.id,{name:req.body.name}, {new :true})
     if (genre){
        res.send(genre)
     }
     else
     return res.status(404).send({
         status:404,
         message:"the genre cannot be found"
     })
 
 })
 
 router.delete('/:id',async (req,res)=>{
     let genre =await Genres.findByIdAndDelete(req.params.id)
     if (genre){
         const index = genres.indexOf(genre)
         genres.splice(index,1)
 
     res.send(genre)
     }
     else
    return res.status(404).send({
         status:404,
         message:"the genre cannot be found"
     })
 })

 
function validategenre(genre){
    const schema ={
        name:Joi.string().min(3).required()
    }
    return result= Joi.validate(genre, schema)
}

 module.exports=router