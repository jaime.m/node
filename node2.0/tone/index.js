
const morgan =require('morgan');
const express =require('express');
const app= express();
require('./startup/routes')(app);
require('./startup/db')();
require('./startup/logginfg')();

const PORT = process.env.PORT||3000;


// this just work with syncrhonous code

//throw new Error('Something have failed');
//const p = Promise.reject(new Error('something failed with a lot of pain!'));

if(app.get('env')==='development'){   
    app.use(morgan('tiny'));
    console.log('Morgan enabled');
}
const server = app.listen(PORT,()=>{
    console.log(`app running on port ${PORT}`);
});

module.exports = server;

