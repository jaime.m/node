
const express =require('express');
const Customer =require('../models/customers')
const router = express.Router();
const validateCustomer =require('../helpers/validateCustomer');

const auth =require('../middleware/auth');

router.get('/',async (req,res)=>{
    const allCustomers =await Customer.find().sort('name');
     res.send(allCustomers);
});

router.get('/:id', async (req,res)=>{
    const customer =await Customer.findById(req.params.id);
    if(!customer)  return res.status(404).send('The customer with the given id was not found');
    res.send( customer);
});

router.post('/',auth,async (req,res)=>{

    const {error}= validateCustomer(req.body);
     if(error){
        res.status(400).send(error.details.map(x=>x.message).join('-'));
        return;
    }
    const newCustomer = new Customer ({
        name: req.body.name,
        isGold: req.body.isGold,
        phone:req.body.phone,
    });
try{
    await newCustomer.save();

}catch(err){
    console.error(err);
}
    res.send( newCustomer);
});

router.put('/:id',async (req,res)=>{

    const customer =await  Customer.findByIdAndUpdate(req.params.id,req.body,{new:true});
    customer.name=req.body.name;
    res.send( customer );
});

router.delete('/:id',async (req,res)=>{
    const customer = await Customer.findByIdAndDelete(req.params.id);
    res.send(customer );
});

module.exports=router;