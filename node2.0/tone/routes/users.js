
const express =require('express');
const Users =require('../models/user');
const jwt =require('jsonwebtoken');
const _ =require('lodash');
const router = express.Router();
const bcrypt =require('bcrypt');
const auth =require('../middleware/auth');
const  mongoose  = require('mongoose');

router.get('/',async (req,res)=>{
    const allUsers =await Users.find();
     res.send(allUsers);
});

router.get('/me',auth, async (req,res)=>{
    
    const user =await Users.findById(req.user._id).select('-password');
    if(!user)  return res.status(404).send('The user with the given id was not found');
    res.send( user);
});

router.post('/',async (req,res)=>{
    const user= await Users.findOne({email:req.body.email});

    if( user) res.status(400).send('User already registered.');

    
    const newUser = new Users (_.pick(req.body,['name','password','email']));
    const salt = await bcrypt.genSalt(10);
    const pass = await bcrypt.hash(newUser.password,salt);
    newUser.password=pass;
    try{
        await newUser.save();
        const token = newUser.generateAuthToken();
        res.header('x-auth-token',token).send( _.pick(newUser,['_id','name','email']));

    }catch(err){
        console.error(err);
        res.status(500).send('something failed ',err)
    }
});

router.put('/:id',async (req,res)=>{

    const user =await  Users.findByIdAndUpdate(req.params.id,req.body,{new:true});
    
    res.send( user );
});

router.delete('/:id',async (req,res)=>{
    const user = await Users.findByIdAndDelete(req.params.id);
    res.send(user );
});

module.exports=router;