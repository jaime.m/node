
const express =require('express');
const Movie =require('../models/movie')
const router = express.Router();
const validateCustomer =require('../helpers/validateCustomer');
const { Genre } = require('../models/genre');
const auth =require('../middleware/auth');


router.get('/',async (req,res)=>{
    const allMovies =await Movie.find().sort('title');
     res.send(allMovies);
});

router.get('/:id', async (req,res)=>{
    const movie =await Movie.findById(req.params.id);
    if(!movie)  return res.status(404).send('The movie with the given id was not found');
    res.send( movie);
});

router.post('/',auth,async (req,res)=>{


    const genre= await Genre.findById(req.body.genre);

    const newMovie = new Movie ({
        title: req.body.title,
        genre: {
            _id:genre._id,
            name:genre.name,
        },
        numberInStock:req.body.numberInStock,
        dailyRentalRate:req.body.dailyRentalRate,
    });
    console.log(newMovie);
try{
    await newMovie.save();

}catch(err){
    console.error(err);
}
    res.send( newMovie);
});

router.put('/:id',async (req,res)=>{

    const movie =await  Movie.findByIdAndUpdate(req.params.id,req.body,{new:true});
    
    res.send( movie );
});

router.delete('/:id',async (req,res)=>{
    const movie = await Movie.findByIdAndDelete(req.params.id);
    res.send(movie );
});

module.exports=router;