
const express =require('express');
const Rental =require('../models/rentals');
const Movie =require('../models/movie');
const Customer =require('../models/customers');
const Fawn =require('fawn');
const router = express.Router();
const validateCustomer =require('../helpers/validateCustomer');
const { Genre } = require('../models/genre');
const  mongoose  = require('mongoose');
const auth =require('../middleware/auth');


Fawn.init(mongoose);
router.get('/',async (req,res)=>{
    const allRentals =await Rental.find().sort('-dateOut');
     res.send(allRentals);
});

router.get('/:id', async (req,res)=>{
    const rental =await Rental.findById(req.params.id);
    if(!rental)  return res.status(404).send('The movie with the given id was not found');
    res.send( rental);
});

router.post('/',auth,async (req,res)=>{
    const customer= await Customer.findById(req.body.customer);
    const movie= await Movie.findById(req.body.movie);



    const newRental = new Rental ({
        title: req.body.title,
        customer: {
            _id:customer._id,
            name:customer.name,
            isGold:customer.isGold,
            phone:customer.phone,
        },
        movie: {
            _id:movie._id,
            title:movie.title,
            dailyRentalRate:movie.dailyRentalRate,
        },
    });
try{

    new Fawn.Task().save('rentals',newRental)
    .update('movies', {_id:movie._id},{$inc:{numberInStock:-1}}).run();

    res.send( newRental);

}catch(err){
    console.error(err);
    res.status(500).send('something failed ',err)
}
});

router.put('/:id',async (req,res)=>{

    const rental =await  Rental.findByIdAndUpdate(req.params.id,req.body,{new:true});
    
    res.send( rental );
});

router.delete('/:id',async (req,res)=>{
    const rental = await Rental.findByIdAndDelete(req.params.id);
    res.send(rental );
});

module.exports=router;