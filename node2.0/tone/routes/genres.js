
const express =require('express');
const {Genre} =require('../models/genre')
const router = express.Router();
const validategenre =require('../helpers/validate');
const auth =require('../middleware/auth');
const admin =require('../middleware/isAdminAuth');
const  asyncMiddleware =require('../middleware/error')

router.get('/',async (req,res)=>{
    const allGenres =await Genre.find().sort('name');
     res.send(allGenres);
});

router.get('/:id', async (req,res)=>{
    const genre =await Genre.findById(req.params.id);
    if(!genre)  return res.status(404).send('The genre with the given id was not found');
    res.send( genre);
});

router.post('/',[auth],async (req,res)=>{

    const {error}= validategenre(req.body);
     if(error){
        res.status(400).send(error.details.map(x=>x.message).join('-'));
        return;
    }
    const newgenre = new Genre ({
        name: req.body.name,
    });
try{
    await newgenre.save();

}catch(err){
    console.error(err);
}
    res.send( newgenre);
});

router.put('/:id',async (req,res)=>{

    const genre =await  Genre.findByIdAndUpdate(req.params.id,req.body,{new:true});
    genre.name=req.body.name;
    res.send( genre );
});

router.delete('/:id',async (req,res)=>{
    const genre = await Genre.findByIdAndDelete(req.params.id);
    res.send(genre );
});

module.exports=router;
