const mongoose =require('mongoose');
const {Schema}=mongoose;
const {genreSchema} =require('./genre');
const { string } = require('joi');

const rentalSchema = new Schema({
    customer: {
        type: new mongoose.Schema({
            name:{
                type:String,required:true,minlength:5,maxlength:50
            },
            isGold:{
                type:Boolean,
                default:false,
            },
            phone:{
                type:String,
                required:true,
                min:5,
                max:30,
            },
        }),required:true,
    },
    movie:{
        type:new mongoose.Schema({
            title:{type:String,required:true,trim:true,minlength:5,maxlength:255},
            dailyRentalRate:{type:Number,required:true,min:0},
        },
        ),required:true,
    },
    dateOut:{
        type:Date,
        required:true,
        default:Date.now,
    },
    dateReturned:{
        type:Date,
    },
    rentalFee:{
        type:Number,
        min:0,
    }
},{
    timestamps:{createdAt:true,updatedAt:true}
});
module.exports = mongoose.model('rental',rentalSchema);