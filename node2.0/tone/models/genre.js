const mongoose =require('mongoose');
const { boolean } = require('joi');
const {Schema}=mongoose;

const genreSchema = new Schema({
    name:{type:String,required:true, minlength:5, maxlength:50},
/*     author:String,
    tags:[String],
    inPublish:boolean, */

},{
    timestamps:{createdAt:true,updatedAt:true}
});

module.exports= {
    Genre:mongoose.model('course',genreSchema),
    genreSchema:genreSchema
}