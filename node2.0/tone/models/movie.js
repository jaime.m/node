const mongoose =require('mongoose');
const {Schema}=mongoose;
const {genreSchema} =require('./genre');

const movieSchema = new Schema({
    title:{type:String,required:true, minlength:5, maxlength:50},
    numberInStock:{
        type:Number,min:0
    },
    dailyRentalRate:{type:Number,min:0},
    genre:{type:genreSchema, required:true},
/*     author:String,
    tags:[String],
    inPublish:boolean, */

},{
    timestamps:{createdAt:true,updatedAt:true}
});
module.exports = mongoose.model('movie',movieSchema);