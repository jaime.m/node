const mongoose =require('mongoose');
const {Schema}=mongoose;
const jwt =require('jsonwebtoken');

const userSchema = new Schema({
    name:{type:String,required:true, minlength:5, maxlength:50},
    password:{type:String, required:true, minlength:4,maxlength:255},
    email:{type:String,required:true,unique:true},
    isAdmin:{type:Boolean,default:false,},
},{
    timestamps:{createdAt:true,updatedAt:true}
});
userSchema.methods.generateAuthToken= function(){
    const token = jwt.sign({_id:this._id,isAdmin:this.isAdmin},'jwtprivatekey');
    return token;
}

module.exports= mongoose.model('user',userSchema)