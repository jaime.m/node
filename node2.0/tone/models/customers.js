const mongoose =require('mongoose');
const {Schema}=mongoose;

const customerSchema = new Schema({
    name:{type:String,required:true, minlength:5, maxlength:50},
    isGold:{type:Boolean,default:false},
    phone:{type:String,required:true,min:6,max:20},


},{
    timestamps:{createdAt:true,updatedAt:true}
});
module.exports = mongoose.model('customer',customerSchema);