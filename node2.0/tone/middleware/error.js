const err=(handler)=>{
    return async (req,res,next)=>{
        try{
            handler(req,res);
        }catch(err){
            next(err);
        }
    }
}
module.exports=err;