const jwt =require('jsonwebtoken');

const auth = (req,res,next)=>{
    const token = req.header('x-auth-token');
    if(!token)return  res.status(401).send('access denied. no token provided');

try{
    const decoded = jwt.verify(token,'jwtprivatekey')
    if(!decoded.isAdmin){
        return  res.status(403).send('User not allowed');
    }
    next();
}catch(err){
    res.status(400).send('invalid token');
}

}

module.exports=auth;