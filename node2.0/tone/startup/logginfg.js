require('express-async-errors');


module.exports= function(){
    process.on('uncaughtException',(ex)=>{
        console.log(`we got an exception here ${ex}`);
        process.exit(1);
    
    })
    process.on('unhandledRejection',(ex)=>{
        console.log(`we got an exception here async ${ex}`);
        process.exit(1);
    })
    
}