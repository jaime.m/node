const genres  =require('../routes/genres');
const customers =require('../routes/customer');
const movies =require('../routes/movies');
const auth =require('../routes/auth');
const home  =require('../routes/home');
const rental  =require('../routes/rental');
const user  =require('../routes/users');
const express =require('express');
const config =require('config')
const helmet =require('helmet');
const errorMiddleware =require('../middleware/error');

module.exports= function(app){
    app.use(express.json());
    app.use(express.static('public'));
    app.use(helmet());
    app.use('/api/genres',genres);
    app.use('/api/customer',customers);
    app.use('/api/movies',movies);
    app.use('/api/rentals',rental);
    app.use('/api/users',user);
    app.use('/api/auth',auth);
    app.use('/api/home',home);
    app.use(errorMiddleware);
}