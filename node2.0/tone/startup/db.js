
const mongoose =require('mongoose');
module.exports = function(){

    mongoose.connect('mongodb://localhost/playground2_test',{useNewUrlParser: true,useUnifiedTopology: true,useFindAndModify:false }).then(()=>{
        console.log('Connected to mongoDb...');
    }).catch((err)=>{
        console.error('Could not connect to MongoDb...', err);
    })
}