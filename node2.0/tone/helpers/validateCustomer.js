const Joi =require('joi');

const validateCustomer =(body)=>{
    const JoiSchema= Joi.object( {
        name:Joi.string().min(3).required().max(50),
        isGold:Joi.boolean().required(),
    phone:Joi.string().required().min(6).max(20),        
    });
    return  JoiSchema.validate(body);
}

module.exports=validateCustomer;