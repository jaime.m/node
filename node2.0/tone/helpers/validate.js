const Joi =require('joi');

const validategenre =(body)=>{
    const JoiSchema= Joi.object( {
        name:Joi.string().min(5).required().max(50),
    });
    return  JoiSchema.validate(body);
}

module.exports=validategenre;