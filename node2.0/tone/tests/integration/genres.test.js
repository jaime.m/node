const request =require('supertest');
const {Genre} =require('../../models/genre')
const { expectCt } = require('helmet');
const User =require('../../models/user');
const mongoose =require('mongoose');
let server;

describe('api/genres',()=>{

    beforeEach(async()=>{
        //We need to do this, because else, the ports is going to be open always
        server = require('../../index');
        await Genre.remove({});

    });

    afterEach(async ()=>{
        //We need to do this, because else, the ports is going to be open always
        //You always have to execute the test in a clean state
        server.close();
    });

    describe('GET', ()=> {

        
        it('Should return all genres',async ()=>{
            
            await Genre.collection.insertMany([
                 {name:"genre1"},
                 {name:"genre2"},
             ]);
            const res= await request(server).get('/api/genres');
            expect(res.status).toBe(200);
            expect(res.body.length).toBe(2);
            expect(res.body.some(g=>g.name==='genre1')).toBeTruthy();
            expect(res.body.some(g=>g.name==='genre2')).toBeTruthy();
                })

    });
    describe('GET/:id',()=>{
        it('Shoud return 404 if the genre with proper id does not exist',async()=>{
            const id= mongoose.Types.ObjectId();
            const res = await request(server).get(`/api/genres/${id}`);
            expect(res.status).toBe(404);
        })
        it('Shoud return the genre with the proper id',async()=>{
          const genre = new Genre({
              name:'genre1'
          });
            genre.save();
            console.log(genre._id);

            const res = await request(server).get(`/api/genres/${genre._id}`);

            expect(res.status).toBe(200);
            expect(res.body).toHaveProperty('name',genre.name);

        })
    })
    describe('POST /',()=>{

        it('should return 401 if client is not logged in',async ()=>{
            const res = await request(server).post('/api/genres').send({name:'genre1'});
            expect(res.status).toBe(401);

        })
        it('should return 400 if genre is less than 5 characters',async ()=>{

            const token = new User().generateAuthToken();
            const res = await request(server)
            .post('/api/genres')
            .set('x-auth-token',token)
            .send({name:'g1'});
            expect(res.status).toBe(400);

        })
        it('should return 400 if genre is more  than 50 characters',async ()=>{

            const name = new Array(60).join('a');
            const token = new User().generateAuthToken();
            const res = await request(server)
            .post('/api/genres')
            .set('x-auth-token',token)
            .send({name});
            expect(res.status).toBe(400);

        })
        it('Should return the genre if it is valid',async()=>{
            const name = 'genre1';
            const token = new User().generateAuthToken();
            const res = await request(server)
            .post('/api/genres')
            .set('x-auth-token',token)
            .send({name});
            expect(res.body).toHaveProperty('_id');
            expect(res.body).toHaveProperty('name',name);
        })        
        it('Should save the genre if it is valid',async()=>{
            const name = 'genre1';
            const token = new User().generateAuthToken();
            const res = await request(server)
            .post('/api/genres')
            .set('x-auth-token',token)
            .send({name});
            const genre = await Genre.find({name:name});
            console.log(genre);
            expect(genre[0].name).toBe(name);
        })        
    });


})