let server;
const Rental =require('../../rental');
const request =require('supertest');
const mongoose =require('mongoose');

describe('/api/returns',()=>{
    beforeEach(async ()=>{
    server=request('../../index.js');  
    const rental= new Rental({
        customer:{
            _id:mongoose.Types.ObjectId(),
            name:'12345',
            phone:'12345'
        },
        movie:{
            _id:mongoose.Types.ObjectId(),
            title:'12345',
            dailyRentalRate:2,
        }
    });   
    await rental.save();   
    await Rental.remove({});
    })
    
    afterEach(()=>{
        server.close(); 
    })
    if('should work',async ()=>{

        const allRentals= await rental.find();
        expect(allRentals.length).toBe(1);

    })

})