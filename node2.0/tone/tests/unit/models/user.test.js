const User =require('../../../models/user');
const jwt =require('jsonwebtoken');
const mongoose =require('mongoose');

describe('user.generateAuthToken',()=>
{
it('Should  return a valid JWT',()=>{
    const id=mongoose.Types.ObjectId();
    const user = new User({_id:id.toHexString(),isAdmin:true});
    const token = user.generateAuthToken();
    const decoded = jwt.verify(token,'jwtprivatekey');
    expect(decoded).toMatchObject({_id:id.toHexString(),isAdmin:true});
})
})