// POST /api/returns {cid,moid}

/*
return 401 if clients is not logged in
return 400 if customer is not providen
return 400 if movie is not providen
return 404 if no rental found
return 400 if rental already processed
return 200 if valid request
set the return date
calculate the rental fee
increase the stock
return the rental
*/