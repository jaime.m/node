const exercise1 =require('../exercise1');

describe('Fizzbuz',()=>{
    it('Should throw if the input is not a number',()=>{

        const args=[null,undefined,NaN,'hola',false];

        args.map((x)=>{
            expect(()=>{
                lib.registerUser(x)
            }).toThrow();
        });
    })
    it('Should return FizzBuzz if the input is divisible by 3 and 5',()=>{
        const result = exercise1.fizzBuzz(15);
        expect(result).toBe('FizzBuzz')
    })
    it('Should return Fizz if the input is divisible by 3 and not by 5 ',()=>{
        const result = exercise1.fizzBuzz(9);
        expect(result).toBe('Fizz')
    })
    it('Should return FizzBuzz if the input is divisible by 5 and not by 3',()=>{
        const result = exercise1.fizzBuzz(10);
        expect(result).toBe('Buzz')
    })
    it('Should return the same number if the input is not divisible by 3 nor 5 ',()=>{
        const result = exercise1.fizzBuzz(14);
        expect(result).toBe(14)
    })

})