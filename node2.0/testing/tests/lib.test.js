const lib = require('../lib');
const db = require('../db');
const mail = require('../mail');

describe('absolute function',()=>{
    
    it(' should return a positive number if input is positive',()=>{
        const reusult = lib.absolute(1);
    
        expect(reusult).toBe(1);
    });
    
    it('should return a positive number if input is negative',()=>{
        const reusult = lib.absolute(-1);
    
        expect(reusult).toBe(1);
    })
    it('should return 0 if input is 0',()=>{
        const reusult = lib.absolute(0);
    
        expect(reusult).toBe(0);
    })
})

describe('greet',()=>{
    it('Should return the greeting message',()=>{

        const result = lib.greet('mosh');
        expect(result).toMatch(/mosh/);
        expect(result).toContain('mosh');

    })

});

describe('getCurrencies',()=>{
    it('Should return supported currencies',()=>{
        const result = lib.getCurrencies();
        //too general
        //expect(result).toBeDefined();
        //expect(result).notToBeNull();
        //to specific}
        //expect(result[0]).toBe('USD');
        //expect(result[1]).toBe('AUD');
        //expect(result[2]).toBe('EUR');
        //proper way
        //expect(result).toContain('USD')
        //expect(result).toContain('AUD')
        //expect(result).toContain('EUR')
        //cleaner way!
        expect(result).toEqual(expect.arrayContaining(['EUR','USD','AUD']))


    })
})

describe('gerProduct',()=>{
    it('Should return product with the given id',()=>{
        const result = lib.getProduct(1);
        
        //if i made the comparisson like this is going to failt, because those objects are in diferent space in memory
        // to be compare the reference of those 2 object in memory
        //expect(result).toBe({id:1,price:10})
        expect(result).toEqual({id:1,price:10});
        expect(result).toMatchObject({id:1,price:10});//like a regular expresion with those fields
        expect(result).toHaveProperty('id',1)
    })
})

describe('registerUser',()=>{
    it('Should throw if username is falsy',()=>{
            //null, undefined NaN '' 0 false
        const args=[null,undefined,NaN,'',0,false];

        args.map((x)=>{
            expect(()=>{
                lib.registerUser(x)
            }).toThrow();
        });
    })
    it('should return user object if valid user is passed',()=>{
        const result=lib.registerUser('mosh');
        expect(result).toMatchObject({username:'mosh'});
        expect(result.id).toBeGreaterThan(0);

    })
})

describe('applyDiscount',()=>{
    it('should apply 10% of discount if customer has more than 10 points',()=>{

        db.getCustomerSync=
        (customerId)=>{
            console.log('fake reading customer');
            return {
                id:customerId,
                points:42
            }
        }
        const order ={customerId:1,totalPrice:10};
        lib.applyDiscount(order);
        
        expect(order.totalPrice).toBe(9);

    })

})

describe('notifyCustomer',()=>{
    it('should send an email to the customer',()=>{

      const mockFunction=   jest.fn();
      //mockFunction.mockResolvedValue(1);
     // mockFunction.mockRejectedValue(new Error('error'));
     // mockFunction.mockReturnValue(1);

 /*        db.getCustomerSync=
        ()=>{ console.log('fake reading customer');
            return {
                email:'a'
            }
        } */

        db.getCustomerSync =jest.fn().mockReturnValue({email:'a'});

       // let mailSent=false;
        mail.send= jest.fn()
     /*    mail.send=()=>{
          mailSent=true
        } */
        const customer={customerId:1}
        lib.notifyCustomer( customer);

        //expect(mailSent).toBe(true);
        expect(mail.send).toHaveBeenCalled();
        expect(mail.send.mock.calls[0][0]).toBe('a')
        expect(mail.send.mock.calls[0][1]).toMatch(/order/);
        
        //expect(mail.send).toHaveBeenCalledWith('a','...');//Best with no strings, numbers!

    })
})