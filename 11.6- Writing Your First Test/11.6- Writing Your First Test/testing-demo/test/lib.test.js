const lib  =require('../lib')
const ex1  =require('../exercise1')
const db =require('../db')
const mail =require('../mail')
describe('absolute',()=>{

    it('should return  positive number if input is poditive',()=>{
      //Matcher functions equal, less tobeclose, etc
        const result = lib.absolute(1)
       expect(result).toBe(1)
    })
    it('should return  negative number if input is negative',()=>{
        //Matcher functions equal, less tobeclose, etc
        const result = lib.absolute(-1)
        expect(result).toBe(1)
    })
    it('should return  0  if input is 0',()=>{
        //Matcher functions equal, less tobeclose, etc
        const result = lib.absolute(0)
        expect(result).toBe(0)
    })
})

describe('greet',()=>{
    it('Should return the greeting message',()=>{
        const result=lib.greet('Copy')
        //expect(result).toMatch(/Copy/)
        expect(result).toContain('Copy')

    })
})

describe('getCurrencies',()=>{
    it('Should return the supported currencies',()=>{
        const result=lib.getCurrencies()
        //expect(result).toMatch(/Copy/)
        //expect(result).toBeDefined()
        
        //proper way
        expect(result).toContain('USD')
        expect(result).toContain('AUD')
        expect(result).toContain('EUR')

        //ideal way
        expect(result).toEqual(expect.arrayContaining(['EUR','AUD','USD']))

    })
})
/*
describe('getProduct',()=>{
    it('Should return the product with the given id',()=>{
        const result=lib.getProduct(1)

        //ideal way
        expect(result).toEqual({
            id:1,price:10
        })*//*
        expect(result).toMatchObject({
            id:1,price:10
        })
        expect(result).toHaveProperty('id',1)

    })
})*/

describe('registerUser',()=>{
    it('Should throw if username is falsy',()=>{
        //Null
        //undefined
        //NaN
        //''
        //0
        //false

        const args =[null, undefined,NaN,'',0,false]
        args.forEach(a=>{

            expect(()=>{
                lib.registerUser(a)
            }).toThrow()
        })

    })
    it('Should return a userr object if valid username is passed',()=>{
        const result = lib.registerUser('Copy')

        expect(result).toMatchObject({
            username: 'Copy'
        })
        expect(result.id).toBeGreaterThan(0)

    })
})

describe('fizzBuzz',()=>{

    it('Should throw an exception if input is not a number',()=>{
            expect(()=>{
                ex1.fizzBuzz('string')
            }).toThrow()
            expect(()=>{
                ex1.fizzBuzz(null)
            }).toThrow()
            expect(()=>{
                ex1.fizzBuzz(undefined)
            }).toThrow()
            expect(()=>{
                ex1.fizzBuzz()
            }).toThrow()
    })

    it('should return  FizzBuzz if input is divisible by 3 and 5 ',()=>{
      //Matcher functions equal, less tobeclose, etc
        const result = ex1.fizzBuzz(15)
       expect(result).toBe('FizzBuzz')
    })
    it('should return  Fizz if input is is divisible by 3 ',()=>{
        const result = ex1.fizzBuzz(3)
        expect(result).toBe('Fizz')
    })
    it('should return  Buzz if input it is divisible by 5',()=>{
        const result = ex1.fizzBuzz(5)
       expect(result).toBe('Buzz')
    })
    it('should return  the input if it is not divisible by 3 or 5',()=>{
        //Matcher functions equal, less tobeclose, etc
        const result = ex1.fizzBuzz(16)
        expect(result).toBe(16)
    })
})

describe('applyDiscount',()=>{
    it('should apply 10% discount if customer has more than 10 points',()=>{
        
        db.getCustomerSync=function(customerId){
            console.log('Fake reading customer...');
            return { id: customerId, points: 20 };
        }

        const order ={
            customerId:1,totalPrice:10
        }
        lib.applyDiscount(order)

        expect(order.totalPrice).toBe(9)        
    })
})

describe('notifyCustomer',()=>{
    it('should send an email to the customer',()=>{
        

        const mockFunction= jest.fn()
        //mockFunction.mockReturnValue(1)
        //mockFunction.mockResolvedValue(1)
        //mockFunction,mockRejectedValue(new Error('Error'))
        //const result=await mockFunction()

        db.getCustomerSync=jest.fn().mockReturnValue({email:'a'})
        mail.send=jest.fn()
           /* db.getCustomerSync=function(customerId){
            console.log('Fake reading customer...');
            return { email:'a' };
        }
        let mailSent=false;
        mail.send = function(email,message){
            mailSent=true
        }*/
        lib.notifyCustomer({customerId:1})

        //expect(mailSent).toBe(true)        
        expect(mail.send).toHaveBeenCalled()
        //verifying parameters
        expect(mail.send.mock.calls[0][0]).toBe('a')
        expect(mail.send.mock.calls[0][1]).toMatch(/order/)
    })
})

