
const express = require('express');
const router = express.Router();
const {Rental}  =require('../models/rental')
const moment =require('moment')
const Joi =require('joi')
const {Movie}  =require('../models/movie')
const auth=require('../middleware/auth')
const validate =require('../middleware/validate')


router.post('/',[auth,validate(validateReturn)], async (req, res) => {

/*  starting to use joi 
    if(!req.body.customerId) return res.status(400).send('customerId not provided')
    if(!req.body.movieId) return res.status(400).send('movieId not provided')
 */

 //static methods available in the class: Rental.lookup
 //instance methods new User().generateAuthToken
const rental= await Rental.lookup(req.body.customerId,req.body.movieId)

/*    const rental= await Rental.findOne({
        'customer._id':req.body.customerId,
        'movie._id':req.body.movieId
    }) */
    if(
        !rental
    )return res.status(404).send('rental not found')
    if(rental.dateReturned)return res.status(400).send('rental already returned')
/* 
    rental.dateReturned=new Date()
    const rentalDayls= moment().diff(rental.dateOut,'days')
    rental.rentalFee= rentalDayls *rental.movie.dailyRentalDate */
    rental.return()
    await Movie.update({_id:rental.movie._id},
       { $inc:{numberInStock:1}}
        )
    await rental.save()
    return res.send(rental)    
});

function validateReturn(req) {
    const schema = {
      customerId: Joi.ObjectId().required(),
      movieId:   Joi.ObjectId().required(),
    };
  
    return Joi.validate(req, schema);
  }
module.exports=router