//POST /api/returns {customerId, movieId}

const server = require('../..')
const { Genre } = require('../../models/genre')

//Return 401 if clients is not logged in
//return 400 if customerId in not providen
//return 400 if movie Id is not providen
//return 404 if no rental found for this custormer/movie
//return 400 if rental already processed
//return 200 if it is a valid request
//Set the return date
//calculate the rental fee
//Increase the stock of movie
//Return the rental
const mongoose =require('mongoose')
const moment =require('moment')
const {Rental}= require('../../models/rental')

const {Movie}= require('../../models/movie')
let customerId,movieId
const{User}  =require('../../models/user')
const request  =require('supertest')
const { Movie } = require('../../models/movie')
let rental
let movie
let token

describe('/api/returns',()=>{
    let server
        const exec=()=>{
            return  request(server)
            .post('/api/returns')
            .set('x-auth-token',token)
            .send({customerId, movieID})
            expect(res.status).toBe(400)
        }
   beforeEach(async ()=>{
       customerId=mongoose.Types.ObjectId()
       movieId=mongoose.Types.ObjectId()
       const token = new User().generateAuthToken()

       server= require('../../index')
       movie = new Movie({
           _id:movieId,
           title:"movie title",
           dailyRentalRate:2,
           genre:{name:'222312'},
           numberInStock:10
       })
       await movie.save()
        rental = new Rental({
           customer:{
               _id:customerId,
                name:'customerName',
                phone:'12312312'  
            },
            movie:{
                _id:movieId,
                title:"movie title",
                dailyRentalRate:2
            }
       })
       await rental.save()

   }) 
   afterEach(async ()=>{
      await server.close()
       await Rental.remove({})
       await Movie.remove({})
   })

   it('should return 401 if client is not logged in',async ()=>{
    token=''
    const res = await  exec()
    expect(res.status).toBe(401)
   })
   it('should return 400 if client is not provided',async ()=>{
    customerId=''
    //delete payload.customerId
    const res = await  exec()
    expect(res.status).toBe(400)
   })

   it('should return 400 if movieID is not provided',async ()=>{
   movieId=''
   const res = await  exec()
    expect(res.status).toBe(400)
   })

   it('should return 404 if no rental found for this custormer/movie',async ()=>{
    /* movieId='234'
    customerId='aef' */
    await Rental.remove({})
    const res = await  exec()
     expect(res.status).toBe(404)
    })

    it('should return 400 if the rental is already processed',async ()=>{
        const res = await  exec()
        rental.dateReturned= new Date()
        await rental.save()
        const res = await  exec()

         expect(res.status).toBe(400)
    })
    it('should return 200 if the rental is ok',async ()=>{
        const res = await  exec()
         expect(res.status).toBe(200)
    })

    it('should set the return date if input is valid',async ()=>{
        const res = await  exec()
        const rentalInDb= await Rental.findById(rental._id)
        const dif=new Date()-rentalInDb.dateReturned
         expect(dif).toBeLessThan(10*1000)
    })
    it('should set the rentalFee date if input is valid',async ()=>{
        rental.dateOut= moment().add(-7,'days').toDate()
        await rental.save()
        const res = await  exec()
        const rentalInDb= await Rental.findById(rental._id)
        expect(rentalInDb.rentalFee).toBe(14)
    })

    it('should Increase the movie stock',async ()=>{
        const res = await  exec()

        const movielInDb= await Movie.findById(movieId)
        expect(rentalInDb.numberInStock).toBe(movie.numberInStock+1)
    })

    it('should return the rental in the body of the response',async ()=>{
        const res = await  exec()
        const rentalInDb=await Rental.findById(rental._id)
        /* expect(res.body).toHaveProperty('dateOut')
        expect(res.body).toHaveProperty('dateReturned')
        expect(res.body).toHaveProperty('rentalFee')
        expect(res.body).toHaveProperty('customer')
        expect(res.body).toHaveProperty('movie') */
        expect(Object.keys(res.body)).toEqual(expect.arrayContaining(['dateOut','dateReturned','rentalFee','customer','movie']))
    })
    

})