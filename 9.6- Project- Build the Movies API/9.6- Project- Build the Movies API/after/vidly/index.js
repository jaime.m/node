const express = require('express');
const app = express();
const winston =require('winston')


require('./startup/logging.startup')()
require('./startup/routes')(app)
require('./startup/db.startup')()
require('./startup/config.startup')()
require('./startup/validation.startup')()



const port = process.env.PORT || 3000;
app.listen(port, () => 
winston.info(`Listening on port ${port}...`));