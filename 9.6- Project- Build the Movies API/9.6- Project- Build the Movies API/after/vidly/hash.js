const bcrypt=require('bcrypt')
async function run(){
    const salt=await bcrypt.genSalt(20)
    //1234 --> abcd 
    const hash= await bcrypt.hash('1234',salt)
    console.log(salt);
    console.log(hash);
}
run()