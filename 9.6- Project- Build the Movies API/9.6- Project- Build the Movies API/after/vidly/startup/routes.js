
const express = require('express');
const customers = require('../routes/customers');
const movies = require('../routes/movies');
const rentals= require('../routes/rental.route')
const users= require('../routes/user.routes')
const login = require('../routes/login.route')
const genres = require('../routes/genres');
const efailurerr = require('../middleware/error.middleware')


module.exports=function(app){
    app.use(express.json());
    app.use('/api/genres', genres);
    app.use('/api/customers', customers);
    app.use('/api/movies', movies);
    app.use('/api/rentals', rentals);
    app.use('/api/users', users);
    app.use('/api/login', login);
    app.use(efailurerr)

}