const Joi = require('joi');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken')
const config= require('config')
const userSchema=new mongoose.Schema({
  name:{
  type:string,
  required:true,
  minlength:2,
  maxlength:50
  },
      email:{
      type:string,
      required:true,
      unique:true,
      minlength:5,
      maxlength:255,
  
  }
  ,password:{
      type:string,
      required:true,
      minlength:5,
      maxlength:1024
  },
isAdmin:Boolean
  })
const User = mongoose.model('User', userSchema);


userSchema.method.generateAuthToken=function(){
  const token=jwt.sign({_id:this._id,isAdmin:this.isAdmin},config.get('jwtPrivateKey'))

}
function validateUser(user) {
  const schema = {
    name: Joi.string().required().min(5).max(50),
    email: Joi.string().required().min(5).max(50).email(),
    password: Joi.string().min(5).max(255).required() 
  };

  //Should you see about joi password complexity
  return Joi.validate(user, schema);
}

exports.User = User; 
exports.validate = validateUser;