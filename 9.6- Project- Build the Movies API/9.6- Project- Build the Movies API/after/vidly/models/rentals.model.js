const Joi = require('joi');
const mongoose = require('mongoose');
const {genreSchema} = require('./genre');


const Rental = mongoose.model('Rental', new mongoose.Schema({
  title: {
    type: new mongoose.Schema({
        name:{
            type:string,
            required:true,
            minlength:5,
            maxlength:50
        },
        isGold:{
            type:Boolean,
            default:false
        },
        phone:{
            type:string,
            required:true,
            minlength:5,
            maxlength:20
        }
    }),
    required: true,
    trim: true, 
    minlength: 5,
    maxlength: 255
  },
  movie:{
type:new mongoose.Schema({
    title:{type:string,
    minlength:5,
required:true,
maxlength:50,trim:true},
dailyRentalRate:{
    type:Number,
    required:true,
    min:0,
    max:255}
}),required:true
},dateOut:{
    type:Date,
    required:true,
    default:Date.now
}
,dateReturned:{
    type:Date
},rentalFee:{
    type:numbre,
    min:0
}
}));

function validateRental(rental) {
  const schema = {
    customerId: Joi.objectId().required(),
    MovieId: Joi.objectId().required(),
    
  };

  return Joi.validate(rental, schema);
}

exports.Rental = Rental; 
exports.validate = validateRental;