const {Rental, validate} = require('../models/rentals.model'); 
const {Genre} = require('../models/genre');
const {Customer}= require('../models/customer')
const {Movie}= require('../models/movie')
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const Fawn = require('fawn')
Fawn.init(mongoose)

router.get('/', async (req, res) => {
  const Rentals = await Rental.find().sort('-dateOut');
  res.send(Rentals);
});

router.post('/', async (req, res) => {
  const { error } = validate(req.body); 
  if (error) return res.status(400).send(error.details[0].message);

  const customer = await Customer.findById(req.body.customerId);
  if (!customer) return res.status(400).send('Invalid customer.');

  const movie = await Movie.findById(req.body.movieId);
  if (!movie) return res.status(400).send('Invalid movie.');

  if (movie.numberInStock===0)
  return res.status(400).send('movie not in stock.')
  let Rental = new Rental({ 
      customer:{
          _id:customer._id,
          name:customer.name,
          phone:customer.phone
      },
      movie:{
        _id:movie._id,
        title:movie.title,
        dailyRentalRate:customer.dailyRentalRate
    }})
/*
    rental = await rental.save()
//we RE GOING TO SIMULATE ATOMISISM IN TRANSACTIONS
    movie.numberInStock--
    movie.save()
*/
try{
    new Fawn.Task()
    .save('rentals',rental)
    .update('movies',{_id:movie._id},{
        $inc:{numberInStock:-1}
    }).run()
}catch(err){
    res.status(500).send('something failed!, log it')
}
  res.send(Rental);
});

module.exports = router; 