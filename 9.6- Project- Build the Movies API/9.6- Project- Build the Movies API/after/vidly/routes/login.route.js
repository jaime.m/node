const {User} = require('../models/user.model');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const Joi = require('joi')
const jwt = require('jsonwebtoken')
const _=require('lodash')
const config=require('config')
const bcrypt=require('bcrypt')
router.post('/', async (req, res) => {
    const { error } = validate(req.body); 
    if (error) return res.status(400).send(error.details[0].message);

    let user=  await User.findOne({
        email:User.body.email
    })
    if (!user){
        return res.status(400).send ('Invalid email or password')     
    }

    const validPasswrord=await bcrypt.compare(req.body.password,user.password)
   if(!validPasswrord)
   return res.status(400).send ('Invalid email or password')     

   const token= user.generateAuthToken()
       res.send(
       token
    );
});


function validateUser(req) {
    const schema = {
      email: Joi.string().required().min(5).max(50).email(),
      password: Joi.string().min(5).max(255).required() 
    };
  
    //Should you see about joi password complexity
    return Joi.validate(req, schema);
  }

module.exports=router