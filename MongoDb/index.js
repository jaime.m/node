const mongoose = require('mongoose')
const {Schema} =mongoose
//comparison operators 
//eq equal
//ne not equal
//gt treater than
//grater or equal than
//lt less than
//lte less or equal than
//in 
//nin not in

mongoose.connect('mongodb://localhost/playground').then(()=>{
    console.log('connected to mongo db');
}).catch(err =>{
    console.error('error');
})


const courseSchema = new Schema({
    name:{type:String,
        required:true,
    minlength:5,
maxlength:255,
lowercase:true,
trim:true
//you can put a regular expressión
},
category:{
type:String,
enum:['web','12'],
required:true
},
    author:String,
    tags:{
        type:Array,
        validate:{
           /* 
           custom validatorvalidator:function(v){
                return v&& v.length>0
            },
            message:'course should have at least une tag'*/
            //async validator
            validator:function(v,callback){
                setTimeout(() => {
                    const result=v && v.length>0
                    callback(result)
                }, 2000);
               
            },
            message:'course should have at least une tag'
        }
    },
    date:{type:Date,default:Date.now},
    isPublished:Boolean, 
    value:{type:Number, 
    required:function(){
        return this.isPublished
    },
min:20,
max:200,
get:v=>Math.round(v),
set:v=>Math.round(v)}
})

const Course =mongoose.model('course',courseSchema)

async function createCourse(){

    const course = new Course({
        name:"copy course React",
        author:"copyta",
        tags:["front ","yeah"],
        isPublished:true,
        value:12
    })
    
    const result = await course.save()
    //console.log(result);

}    

async function getCourses(){
//real world they are part of the querystring
    const pageNumber=2
    const pageSize=10

    const info = await Course.
    find({
      //  author:"copyta",
        //isPublished:true
       // value:{$gte:10, $lte:20}
       //value :{$in: [1,13]}
       //^string starts with something
       //$ ends with that mojica$
       //.*copy.* exists in the string 
       //if u want key non sensitive pit a i after the /
       author:/^Copy/
    })
   // .or([{author:"copyta"},{isPublished:true}])
  // .skip(pageNumber-1)*pageSize  //this is for pagination
  //  .limit(pageSize)
    .sort({name:1})
    .select({
        name:1,tags:1
    })
    //we can use the count option instead select option to count the result
    console.log(info);
    
}

async function update(id){
    //approach queryfirst
//findbyid, modifyits properties-->save
/*const course = Course.findByIdAndDelete(id)
if (!course){
    return
}
course.isPublished=true
course.author="jaime"

const info = await course.save()


return info*/


//update first
//get the updated document
//findbyidandupdate
const course = Course.update({_id:id},{
$set:{
    author:"mosh",
    isPublished:false
}
}
,{new:true}
)
return course
}

async function remove(id){
   
const course = Course.deleteOne({_id:id})

return course
}
//createCourse()
//getCourses()




