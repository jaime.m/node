# Node

Node training from mosh

Desing patterns form node.js
Singletons/observers/factories.
What is a desing pattern?
Reusable solution to a commonly problem.
#Singleton
    Resstrict the number of instantiations of a class to one!
        make singletons in node is easy, example:
        //area.js
        var PI =Math.PI;
        function circle (radius){
            return redius*radius*PI
        }
        module.exports.circle=circle
    Doesn't matter how many times do you need this module in the app, it will only exist
    in a single instance
    
    var areaCalc = require('./area')
    console.log(areaCalc.circle(5))
    
#observers
An object maintains a list of dependents/observers and notifies them**

automatically on state changes. to implement the observer pattern ]EventEmitter comes to the rescue

//MyFancyObservable.js
var util = require('util')
var EventEmitter = require('events').EventEmitter

function MyFancyObservable() {
  EventEmitter.call(this);
}
#Factories
Teh factory patter is a creational patter that doesn't require us to use a 
constructor but provides a generic interface for creating objects. this patters can be
really usefull when the creation process is complex

function MyClass (options) {
  this.options = options;
}

function create(options) {
  // modify the options here if you want
  return new MyClass(options);
}

module.exports.create = create;

factories also make testing easier, as you can inject the modules dependencies using this
pattern

#Dependency injection
Dependency injection is a software patter in which one or more dependencies(or services)
are injectes, or passed by reference, into a dependent object

this is helfull because you can easily inject a fake db for unit testing

#middleware/pipelines
So basically when you add a middleware it just gets pushed into a middleware array. So far so good, but what happens when a request hits the server
No magic - your middlewares get called one after the other.

#Streams

can think of streams as special pipelines. They are better at processing bigger amounts of flowing data, even if they are bytes, not objects.

process.stdin.on('readable', function () {
    var buf = process.stdin.read(3);
    console.dir(buf);
    process.stdin.read(0);
});

