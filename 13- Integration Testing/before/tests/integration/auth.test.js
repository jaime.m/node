const request =require('supertest')
const {User}  =require('../../models/user')
const {Genre}  =require('../../models/genre')

describe('auth middleware',()=>{
    
    let token
    const exce=()=>{
         
        await request(server)
        .post('/api/genres')
        .set('x-auth-token',token)
        .send({name:'genre1'})

    }
    beforeEach(()=>{
        token = new User().generateAuthToken()
        server =require('../../index')
    })
    afterEach(async ()=>{
        await Genre.remove({})
        server.close()

    })
    it('should return 401 if token is not provided',async ()=>{

        token =''
        const res= await exce()

        expect(res.status).toBe(401)
    })
    it('should return 400 if invalid token is invalid',async ()=>{

        token ='a'
        const res= await exce()

        expect(res.status).toBe(400)
    })

    it('should return 200 if token is valid',async ()=>{
        const res= await exce()

        expect(res.status).toBe(200)
    })
})