let server
const requeest =require('supertest')
const  {Genre} =require('../../models/genre')
const {User}  =require('../../models/user')
describe('/api/genres',()=>{

    beforeEach(()=>{
        server =require('../../index')
    })
    afterEach(async ()=>{
            //It will remove all the information in genres collection, because
            //in every try it will add 2 new genres
            await Genre.remove({})
        server.close()
    })
    describe('GET /', ()=>{
        it('Should return all genres', async()=>{
           await Genre.collection.insertMany([
               { name:'genre1'},
               {name:"genre2"}
            ])


            const res= await requeest(server).get('/api/genres')
            expect(res.status).toBe(200)

            expect(res.body.length).toBe(2)
            expect(res.body.some(g=>g.name==='genre1')).toBeTruthy()
            expect(res.body.some(g=>g.name==='genre2')).toBeTruthy()

        })
    })
    describe('GET /:id',()=>{
        it('Should return a genre if valid id is passed',()=>{
            
         const genre = new Genre({
             name:"genre1"
         })
         await genre.save()

         const res=await requeest(server).get('/api/genres/'+genre._id)
         expect(res.status).toBe(200)
         expect(res.body).toHaveProperty('name',genre.name)
   

        })
        it('Should 404 if invalid id is passed',()=>{
           const res=await requeest(server).get('/api/genres/1')
           expect(res.status).toBe(404)
       
    })
    })
    describe('POST /', ()=>{

        //Define the happy path, and then in each test, we change
        //one parameter than clearly aligns with the name of
        //the test

        let token
        let name
        const exec = async ()=>{
           return await   requeest(server)
            .post('/api/genres')
            .set('x-auth-token',token)
            .send({name})
        }
        beforeEach(()=>{
             token = new User().generateAuthToken()
             name='genre1'
        })
        it('Should return a 401 if client is not logged in',async ()=>{
          token=''
           const res= await  exec()

            /*const res= await   requeest(server)
            .post('/api/genres')
            .send({name:"genre1"})*/
         expect(res.status).toBe(401)
        })
        it('Should return a 400 if genre is less than 5 characters',async ()=>{
            name='1234'
            const res= await exec()
            expect(res.status).toBe(400)
          })
          it('Should return a 400 if genre is greater than 15 characters',async ()=>{
            name= new Array(54).join('a')
            const res= await exec()
            expect(res.status).toBe(400)
          })
          it('Should save the genre if it is valid',async ()=>{

            const res= await   exec()

              const genre = await Genre.find({name:'genre1'})
           expect(genre).not.toBeNull()
          })

          it('Should return it if  is valid',async ()=>{
             
            const res= await exec()
           expect(res.body).toHaveProperty('_id')
           expect(res.body).toHaveProperty('name','genre1')
        
          })
          
    })
})