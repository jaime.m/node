const mongoose =require('mongoose')
module.exports= function (res,req,next){
    if(!mongoose.Types.ObjectId.isValid(req.param.id))
    return res.status(404).send('invalid id.');

    next()
}