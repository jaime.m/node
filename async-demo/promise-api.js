//This is usefull in unit testing.

const p=Promise.resolve({id:1})
p.then(result=>console.log(result)
)
/*
const p2=Promise.reject(new Error('error'))
p2.catch(result=>console.log(result)
)*/

const p1 = new Promise((resolve)=>{
    setTimeout(() => {
        console.log('async op 1');
        resolve(1)
        
    }, 2000);
})

const p2 = new Promise((resolve)=>{
    setTimeout(() => {
        console.log('async op 2');
        resolve(2)
        
    }, 2000);
})

Promise.all([p1,p2]).then(result=>console.log('ok')
)
//at least one promise is fullfilled
Promise.race([p1,p2]).then(result=>console.log('ok')
)